;##############################################################################
;#  Copyright (C) 2013-2014 Jagrati Singh (singh.jagriti5@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################


; Argentina has submitted an application to host the World Cup.[cambridge dict]
;अर्जेंटीना ने विश्व कप की मेजबानी के लिए एक आवेदन-पत्र जमा किया है.

(defrule submit0
(declare (salience 5000))
(id-root ?id submit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-word ?id1 application|claim|complaint)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jamA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  submit.clp 	submit0  "  ?id "  jamA_kara)" crlf))
)

;In conclusion, I submit that the proposal will not work without some major changes.[cambridge dict]
;निष्कर्ष में, मैं कहता हूँ कि प्रस्ताव कुछ मुख्य परिवर्तनों के बिना काम नहीं करेगा . 
(defrule submit1
(declare (salience 4900))
(id-root ?id submit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) that)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  submit.clp 	submit1  "  ?id "  kaha)" crlf))
)

;...............Default rule...................
; She refused to submit to threats. [oxford advance learner]
;उसने धमकियों को मान लेने से मना कर दिया .  
(defrule submit2
(declare (salience 1))
(id-root ?id submit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  submit.clp 	submit2  "  ?id "  mAna_le)" crlf))
)



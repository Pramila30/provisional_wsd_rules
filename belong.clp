;Modified by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 6-dec-2013
;This book belongs to Sarah.[cambridge]
;यह पुस्तक सराह का है . 
;You shouldn't take what doesn't belong to you.[cambridge]
; जो आपका नहीं है वो आपको नहीं लेना चाहिए  . 
(defrule belong0
(declare (salience 5000))
(id-root ?id belong)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1); Added by Garima Singh
;(id-word =(+ ?id 1) to)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng (+ ?id 1) ?id kA_hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  belong.clp      belong0   "  (+ ?id 1) " "?id" kA_hE)" crlf)
)
)


;Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 6-dec-2013
;They belong to the same chess club.[cambridge]
;वे एक ही शतरञ्ज क्लब के सदसय है
(defrule belong2
(declare (salience 5000))
(id-root ?id belong)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
(id-word ?id1 organization|group|club|party|family)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saxasya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  belong.clp 	belong2   "  ?id "  saxasya )" crlf))
)


;Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 6-dec-2013
(defrule belong3
(declare (salience 5000))
(id-root ?id belong)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-last_word ?id belong)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rakKa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  belong.clp 	belong3   "  ?id "  rakKa_jA )" crlf))
)


;******************DEFAULT RULES**************************

;Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 6-dec-2013
(defrule belong1
(declare (salience 0))
(id-root ?id belong)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samabanXa_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  belong.clp 	belong1   "  ?id "  samabanXa_raKa )" crlf))
)


;*********************EXAMPLES*****************************

;This table belongs in the sitting-room.
;Where do these spoons belong?
;These papers belong with the others.
;After three years in Cambridge, I finally feel as if I belong here.
;This book belongs to Sarah.
;You shouldn't take what doesn't belong to you.
;They belong to the same chess club.

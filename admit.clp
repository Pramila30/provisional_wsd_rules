;##############################################################################
;#  Copyright (C) 2002-2005 Garima Singh (gsingh.nik@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;She's arriving tomorrow, I believe.
;मेरे ख्याल से ,वह कल आ रही है .
(defrule believe0
(declare (salience 3700))
(id-root ?id believe)
?mng <-(meaning_to_be_decided ?id)
(id-last_word ?id believe)
(kriyA-object ?id ?id1)
(id-word ?id1 I)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KyAla_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  believe.clp 	believe0   "  ?id "  KyAla_se )" crlf))
)


;**************************DEFAULT RULES*************************************

;Don't be afraid to admit to your mistakes.[oald]
;अपनी गलतियों को स्वीकार करने से डरो नहीं 
(defrule admit1
(declare (salience 0))
(id-root ?id admit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svIkAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  admit.clp 	admit1   "  ?id "  svIkAra_kara )" crlf))
)




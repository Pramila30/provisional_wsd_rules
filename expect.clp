
(defrule expect0
(declare (salience 5000))
(id-root ?id expect)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 call)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawIkRA_kara))
(assert (kriyA_id-object_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expect.clp 	expect0   "  ?id "  prawIkRA_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  expect.clp    expect0   "  ?id " kI )" crlf)
)
)

;Added by Pramila(Banasthali University) on 25-11-2013
;We are expecting a lot of applicants for the job.        ;cald
;नौकरी के लिए हम बहुत सारे आवेदन की अपेक्षा कर रहें हैं .
(defrule expect1
(declare (salience 4900))
(id-root ?id expect)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apekaRA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expect.clp 	expect1   "  ?id "  apekaRA_kara )" crlf)
)
)

;Added by Pramila(Banasthali University) on 25-11-2013
;I expect you'll find it somewhere in your bedroom. ;cald
;मुझे लगता  है कि तुम्हें वह कही अपने बेडरूम में मिल जायेगा .
;I expect he'd have left anyway.         ;cald
;मुझे लगता है कि वह अब तक चला गया होगा .
(defrule expect2
(declare (salience 4900))
(id-root ?id expect)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma  ?id ?id1)
(kriyA-conjunction  ?id1 ?id2)
(id-word ?id2 that)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expect.clp 	expect2   "  ?id "  laga )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  expect.clp    expect2   "  ?id " ko )" crlf)
)
)

(defrule expect3
(declare (salience 4000))
(id-root ?id expect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ASA_kara))
(assert (kriyA_id-object_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expect.clp 	expect3   "  ?id "  ASA_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  expect.clp    expect3   "  ?id " kI )" crlf)
)
)

;default_sense && category=verb	ASA_raKa	0
;"expect","VT","1.ASA_raKanA"
;We were expecting a visit from our relatives
;
;

;##############################################################################
;#  Copyright (C) 2002-2005 Nandini Upasani (nandini.upasani@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;The setting sun lent an air of melancholy to the scene. [oxford advanced lerner dictionary]
;dUbawe sUrya ne  xqSya ko uxAsI_kI havA ki Jalaka xI.
(defrule lend0
(declare (salience 70))
(id-root ?id lend)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 air)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lend.clp 	lend0   "  ?id " xe )" crlf))
)

;Her presence lent the occasion a certain dignity. [oxford advanced lerner dictionary]
;usakI upasWiwi ne avasara ko koI eka prawirThA praxAna kI.
(defrule lend1
(declare (salience 80))
(id-root ?id lend)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "inanimate.gdbm" ?str)))
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lend.clp 	lend1   "  ?id " praxAna_kara )" crlf))
)

;===========Default-rule==============
;They refused to lend us the money. [oxford advanced lerner dictionary]
;unhoMne hameM pEse uXAra xene se manA kiyA.
(defrule lend2
(declare (salience 60))
(id-root ?id lend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uXAra_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lend.clp 	lend2   "  ?id " uXAra_xe )" crlf))
)